﻿using Microsoft.Data.SqlClient;
using System.Text.RegularExpressions;

namespace PicoPlacaProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            bool continuar = true;

            while (continuar)
            {
                try
                {
                    Console.Write("Ingrese la placa del vehículo (por ejemplo, ABC1234): ");
                    string licenseNumber = Console.ReadLine();

                    if (!IsValidPlacaFormat(licenseNumber))
                    {
                        throw new ArgumentException("La placa no cumple con el formato válido (por ejemplo: ABC1234).");
                    }

                    Console.Write("Ingrese la hora (HH:mm): ");
                    string timeStr = Console.ReadLine();
                    DateTime time = DateTime.ParseExact(timeStr, "HH:mm", null);

                    Console.Write("Ingrese la fecha (dd/MM/yyyy): ");
                    string dateStr = Console.ReadLine();
                    DateTime date = DateTime.ParseExact(dateStr, "dd/MM/yyyy", null);

                    bool canCirculate = CanVehicleCirculate(licenseNumber, date, time);

                    if (canCirculate)
                    {
                        Console.WriteLine("El vehículo puede circular en la hora y fecha ingresadas.");
                    }
                    else
                    {
                        Console.WriteLine("El vehículo no puede circular en la hora y fecha ingresadas debido a Pico y Placa.");
                    }

                    InsertConsultaRecord(licenseNumber, time, date, canCirculate);

                    Console.Write("¿Desea realizar otra consulta? (S/N): ");
                    string respuesta = Console.ReadLine().ToUpper();
                    continuar = (respuesta == "S");
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine("Error: " + ex.Message);
                }
                catch (FormatException)
                {
                    Console.WriteLine("Error: Formato de entrada incorrecto. Por favor, ingrese datos válidos.");
                }
            }
        }

        static bool IsValidPlacaFormat(string licenseNumber)
        {
            return Regex.IsMatch(licenseNumber, @"^[A-Z]{3}\d{4}$");
        }

        static bool CanVehicleCirculate(string licenseNumber, DateTime date, DateTime time)
        {

            int dayOfWeek = (int)date.DayOfWeek;

            bool canCirculate = true;

            if (dayOfWeek == (int)DayOfWeek.Saturday || dayOfWeek == (int)DayOfWeek.Sunday)
            {
                Console.WriteLine("Los fines de semana no hay restricción.");
                
            }

            int lastDigitNumber = int.Parse(licenseNumber.Substring(licenseNumber.Length - 1));
            var restricciones = new Dictionary<int, List<int>>
            {
                { 1, new List<int> { 1, 2 } },  // Lunes
                { 2, new List<int> { 3, 4 } },  // Martes
                { 3, new List<int> { 5, 6 } },  // Miércoles
                { 4, new List<int> { 7, 8 } },  // Jueves
                { 5, new List<int> { 9, 0 } }   // Viernes
            };

            if (restricciones.ContainsKey(dayOfWeek))
            {
                if (restricciones[dayOfWeek].Contains(lastDigitNumber))
                {
                    canCirculate = CanCirculateAtTime(time);
                }
                else
                {
                    Console.WriteLine("El vehículo no tiene restricción vehicular en el día seleccionado.");
                }
            }

            return canCirculate;
        }

        static bool CanCirculateAtTime(DateTime time)
        {
            TimeSpan startMorning = new TimeSpan(6, 0, 0);
            TimeSpan endMorning = new TimeSpan(9, 30, 0);
            TimeSpan startAfternoon = new TimeSpan(16, 0, 0);
            TimeSpan endAfternoon = new TimeSpan(19, 30, 0);

            if ((time.TimeOfDay >= startMorning && time.TimeOfDay <= endMorning) ||
                (time.TimeOfDay >= startAfternoon && time.TimeOfDay <= endAfternoon))
            {
                return false; // No puede circular dentro de estos rangos de tiempo
            }

            return true;
        }

        static void InsertConsultaRecord(string licenseNumber, DateTime time, DateTime date, bool canCirculate)
        {
            string connectionString = "Data Source=CHRISTOPHER\\SQLExpress;Initial Catalog=PicoPlacaDB;Integrated Security=True;TrustServerCertificate=True;";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                string insertQuery = "INSERT INTO Queries (LicenseNumber, Time, Date, CanCirculate) VALUES (@LicenseNumber, @Time, @Date, @CanCirculate)";
                using (SqlCommand command = new SqlCommand(insertQuery, connection))
                {
                    command.Parameters.AddWithValue("@LicenseNumber", licenseNumber);
                    command.Parameters.AddWithValue("@Time", time);
                    command.Parameters.AddWithValue("@Date", date);
                    if (!canCirculate)
                    {
                        command.Parameters.AddWithValue("@CanCirculate", false);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@CanCirculate", true);
                    }


                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
