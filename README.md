<h1>Verificador de Pico y Placa</h1>

Este es un programa en C# que permite verificar si un vehículo puede circular en función de las restricciones de Pico y Placa. El programa solicita al usuario la placa del vehículo, la hora y la fecha, y luego verifica si el vehículo puede circular en ese momento según las siguientes condiciones:


* Las placas con el último dígito 1 y 2 no pueden circular los lunes.
* Las placas con el último dígito 3 y 4 no pueden circular los martes.
* Las placas con el último dígito 5 y 6 no pueden circular los miércoles.
* Las placas con el último dígito 7 y 8 no pueden circular los jueves.
* Las placas con el último dígito 9 y 0 no pueden circular los viernes.

Además, el programa tiene en cuenta los siguientes horarios de restricción:

* No puede circular entre las 6:00 AM y las 9:30 AM.
* No puede circular entre las 4:00 PM y las 8:00 PM.

El resultado de la verificación se muestra en la consola, indicando si el vehículo puede circular en la hora y fecha proporcionadas o si debe abstenerse de circular debido a Pico y Placa.

El programa registra cada consulta en una base de datos SQL Server, almacenando la placa del vehículo, la hora, la fecha y si puede circular o no en ese momento.
